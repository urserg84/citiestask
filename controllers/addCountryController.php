<?php
/**
* Если данные отправлены методом POST и не пусты
*/
if($_SERVER['REQUEST_METHOD'] == 'POST' &&
 isset($_POST['countryField']) && 
 trim($_POST['countryField']) !== '' )

{
	session_start();
	require_once('../autoload.php');
	$secure = new Security();

	$connection = new DBConnect();
	$pdo = $connection->getDBConnect();


    /**
     * Если проверка CSRF не пройдена завершаем выполнение скрипта
     */
    if(!isset($_SESSION['csrf']) || $_SESSION['csrf'] !== $_POST['csrf'])
    {
         die('CSRF attack');
    }

    /**
     * Очистка пользовательского ввода
     */
    $country = $secure->sanitizeinput($_POST['countryField']);

    /**
     * Работа с БД
     */
    $req = $pdo->prepare("INSERT INTO Countries (name)
    VALUES (:name)");
    $req->bindParam(':name', $countryname);
    $countryname = $country; 
    $req->execute();
    echo "Страна добавлена";   
} else 
{
	echo "Вы не ввели страну!";
}


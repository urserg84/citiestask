<?php
session_start();
require_once('autoload.php');
$getToken = new Security();
$key = $getToken->generageToken();
    
require_once('partials/header.php');
?>
<body>
    <div class="main">
        <form class="addCityField" action="controllers/addCountryController.php" method="post">
            <input type="hidden" name="csrf" value="<?php echo $key; ?>" />
            <div class="form-group">
                <input type="text" class="form-control" name="countryField"   placeholder="Введите страну" required>
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn-primary">Добавить</button>
                <a href="countryList.php" class="form-control btn-primary watchlistbtn">Просмотреть список</a>
            </div>
        </form>
    </div>   
<?php
require_once('partials/footer.php');
?>
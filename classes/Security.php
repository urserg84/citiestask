<?php

class Security
{

	/**
     * CSRF защита
     */

	public function generageToken() {
		$key = sha1(microtime());
        $_SESSION['csrf'] = $key;
        return $key;
	}

	/**
     * Очистка пользовательского ввода
     */

	public function sanitizeinput($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
}
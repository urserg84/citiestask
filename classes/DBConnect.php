<?php

class DBConnect 
{
	private $host = '127.0.0.1'; 
	private $db   = 'countriesdb'; 
	private $user = 'root'; 
	private $pass = 'root'; 
	private $opt = [
	    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
	    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	    PDO::ATTR_EMULATE_PREPARES   => false,
	];

	/**
     * Соединение с базой
     */
    
    public function getDBConnect() 
    {
    	try 
    	{
			 $pdo = new PDO("mysql:host=$this->host;dbname=$this->db;charset=utf8", $this->user, $this->pass, $this->opt);
			 return $pdo;
		} 
		catch(PDOException $e)
	    {
	    	echo "Ошибка: " . $e->getMessage();
	    }

	    $pdo = null;
    }
}
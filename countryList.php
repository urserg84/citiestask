<?php
require_once('autoload.php');
$connection = new DBConnect();
$pdo = $connection->getDBConnect();  
$countriesRequest = $pdo->query('SELECT * FROM Countries');
$countries = $countriesRequest->fetchAll(PDO::FETCH_ASSOC); 
    
require_once('partials/header.php');
?>

	<body>
		<table class="table table-hover citytable">
		  <thead>
		    <tr>     
		      <th>Название страны</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php foreach($countries as $row) { ?>
		    	<tr>
			    	<td>
			    		<?= htmlspecialchars($row['name']) ?> 
			    		<a href="#" class="hideElement"> Скрыть</a>
			    	</td>
		    	</tr>
		  	<?php } ?>
		  </tbody>
		</table>
<script src="js/hideScript.js"></script>
<?php
require_once('partials/footer.php');
?>